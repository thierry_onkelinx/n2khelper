**Branch** | **Build status** | **Test coverage**
-- | -- | --
master | [![wercker status](https://app.wercker.com/status/9f6c69b5cc40a32aeffeb5e8ee42b552/s/master "wercker status master branch")](https://app.wercker.com/project/bykey/9f6c69b5cc40a32aeffeb5e8ee42b552) | [![codecov.io](http://codecov.io/bitbucket/thierry_onkelinx/n2khelper/coverage.svg?branch=master)](http://codecov.io/bitbucket/thierry_onkelinx/n2khelper?branch=master)
develop | [![wercker status](https://app.wercker.com/status/9f6c69b5cc40a32aeffeb5e8ee42b552/s/develop "wercker status develop branch")](https://app.wercker.com/project/bykey/9f6c69b5cc40a32aeffeb5e8ee42b552) | [![codecov.io](http://codecov.io/bitbucket/thierry_onkelinx/n2khelper/coverage.svg?branch=develop)](http://codecov.io/bitbucket/thierry_onkelinx/n2khelper?branch=develop)

# The n2khelper package

The `n2khelper` package constains auxiliary functions for the analysis and reporting of the Natura 2000 Monitoring.

It currently holds functions for importing the raw data, creating analysis dataset and running the analysis for the Common Breeding Bird Survey and the Wintering Bird Survey in Flanders.
